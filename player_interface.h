#ifndef PLAYER_INTERFACE_H
#define PLAYER_INTERFACE_H


#include "basic_types.h"


namespace othello
{

  class PlayerInterface {
  public:
    explicit PlayerInterface() = default;
    virtual ~PlayerInterface() = default;

    /*! Ask the player to think ^^,
     *  \param[in] pieces The current piece position bitboard
     *  \param[in] goals  The goal piece position bitboard
     *  \param[in] player_id The current player id to think for
     *  \param[in] max_time The time awailable for thinking
     */
    virtual void think(const BitBoard& pieces, const othello::PlayerId& player_id,
                       const std::chrono::seconds& max_time)
      = 0;

    /*! Query the best move after decided by the player */
    virtual BitPos bestMove() const = 0;

    /*! Query the player type {humen or A.I.} */
    virtual PlayerType type() const = 0;
  };

  class HumanPlayer final : public PlayerInterface {
  public:
    HumanPlayer(const othello::PlayerId&) {}

    /*! Specialication of the think method for humans
     *  We like to think on our own
     *  This statement can not be modified */
    void think(const BitBoard&, const othello::PlayerId&,
               const std::chrono::seconds&) override final
    {
    }

    /*! Specialication of the best move method for humans
     *  No-one needs to know what I think is the best move */
    BitPos bestMove() const override final { return {}; }

    /*! Specialication of the type function returning the player type */
    PlayerType type() const override final { return PlayerType::Human; }
  };

  class AIInterface : public PlayerInterface {
  public:
    /*! Specialication of the type function returning the player type */
    PlayerType type() const override final { return PlayerType::AI; }
  };


}   // namespace othello


#endif   // PLAYER_INTERFACE_H
